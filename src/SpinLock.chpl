/*
  A basic module to provide a spinlock class.  This class allows for separate write locks and
  read handles; while no writelock is active or requested, any number of read handles may be
  opened/activated.  Once a write lock is requested, any task requesting a new read handle will
  yield, and the task requesting the write lock will yield until all active read handles have
  drained to 0.  Only one write lock is allowed at any given time.

  .. highlight:: chapel

  ::
    use SpinLock;
    use FormattedIO;
    var lock: SpinLock = new shared SpinLock();
    var n: int = 0;
    forall i in 0..9 {
      if i == 4 {
        manage lock.write {
          n += 1;
        }
      } else {
        manage lock.read {
          writeln('Hello from task %s!  n is currently %i'.format(i, n));
        }
      }
    }
*/
module SpinLock {
  // in theory, we should never, ever see this error, and if we do, something has
  // gone seriously wrong.
  class TooManyLocksError : Error {
    proc init() { }
  }

  // We can't currently store a reference on a class or record member, so instead
  // we wrap our atomics in a class that share amongst the SpinLock, SpinWrite and
  // SpinRead classes
  class LockHandleWrapper {
    var writeLock: atomic int;
    var readHandles: atomic int;

    proc init() {
      init this;
    }
  }

  /*
    SpinRead class, used to keep track of readHandles on the LockHandleWrapper.  Grants new handles,
    clears old ones, and respects whether or not the writeLock on the LockHandleWrapper has been obtained.
  */
  class SpinRead: contextManager {
    var rlWrapper: shared LockHandleWrapper;

    proc init(ref rlWrapper: shared LockHandleWrapper) {
      this.rlWrapper = rlWrapper;
      init this;
    }
    /*
      A function that creates a new read handle on entering a managed block. If there is an active write
      lock open, this function will cause the task to yield.  NOTE: If you enterContext without a managed
      write lock, you WILL deadlock.
    */
    inline proc enterContext() {
      while this.rlWrapper.writeLock.read() >= 1 do currentTask.yieldExecution();
      this.rlWrapper.readHandles.add(1);
    }
    /*
      A function that decrements the number of read handles, which is called after the manage block ends.
      Normally, you would not call this by itself.
    */
    inline proc exitContext(in err: owned Error?) {
      this.rlWrapper.readHandles.sub(1);
    }
  }

  /*
   SpinLock class, used to actually lock things.  Shares an instance of the LockHandleWrapper to keep track of
   existing read handles and number of existing write locks.  Can be instantianted by itself and used without the
   manage keyword (calling lock and unlock directly).
   */
  class SpinWrite: contextManager {
    var l: atomic bool;
    var n: atomic int;
    var rlWrapper: shared LockHandleWrapper;

    proc init(ref rlWrapper: shared LockHandleWrapper) {
      this.rlWrapper = rlWrapper;
      init this;
    }

    inline proc enterContext() {
      this.rlWrapper.writeLock.add(1);
      while this.rlWrapper.readHandles.read() != 0 do currentTask.yieldExecution();
      try! {
        this.lock();
      }
    }
    inline proc exitContext(in err: owned Error?) {
      this.rlWrapper.writeLock.sub(1);
      try! {
        this.unlock();
      }
    }

    /*
      Callable function that can be used as a simple mutex wrapper, if desired, but is otherwise called by
      the `wl` function (which waits for read handles to drain).  This function yields if the write lock
      is currently held by another task.  If using by itself, make sure to always call `unlock` afterwards.
      This function will throw an error if, for whatever reason, it's not the only existing lock.

      Do _not_ call from within a block of code that has created a read handle that it has yet to release!
    */
    inline proc lock() throws {
      while this.l.testAndSet() do currentTask.yieldExecution();
      this.n.add(1);
      if this.n.read() != 1 {
        throw new owned TooManyLocksError();
      }
    }

    /*
      Callable function which must always be called after `lock`, or else the program will stall.  It reduces
      the number of write handles by 1, throws an error if the number of handles isn't 0, then clears the lock. 
    */
    inline proc unlock() throws {
      this.n.sub(1);
      if this.n.read() != 0 {
        throw new owned TooManyLocksError();
      }
      this.l.clear();
    }
  }

  /*
   SpinLock class; this is almost certainly what you want to call.  Instantiate a new instance, then, with the keyword
   manage, call write and read.  Read handles can be called as many times as desired; however, do not nest read and write
   managements together as this will cause a deadlock.  Instead, ensure that write and read are called in separate blocks of code.
  */
  class SpinLock {
    var l: atomic bool;
    var n: atomic int;
    var t: string;
    var writeLock: atomic int;
    var readHandles: atomic int;
    var rlWrapper: shared LockHandleWrapper;
    var write: owned SpinWrite;
    var read: owned SpinRead;

    proc init() {
      this.writeLock = 0 : atomic int;
      this.readHandles = 0 : atomic int;
      this.rlWrapper = new shared LockHandleWrapper();
      this.write = new owned SpinWrite(this.rlWrapper);
      this.read = new owned SpinRead(this.rlWrapper);
      init this;
    }
  }
}
