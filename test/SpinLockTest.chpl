config const testParam: bool = true;

use SpinLock;

if testParam {
  use IO.FormattedIO; // for the format
  var lock: SpinLock = new owned SpinLock();

  manage lock.write {
    assert(lock.write.n.read() == 1);
    assert(lock.write.rlWrapper.writeLock.read() == lock.rlWrapper.writeLock.read());
    assert(lock.read.rlWrapper.writeLock.read() == lock.rlWrapper.writeLock.read());
    assert(lock.rlWrapper.readHandles.read() == 0);
  }
  manage lock.read {
    assert(lock.write.n.read() == 0);
    assert(lock.rlWrapper.readHandles.read() == 1);
    assert(lock.read.rlWrapper.readHandles.read() == lock.rlWrapper.readHandles.read());
    assert(lock.write.rlWrapper.readHandles.read() == lock.rlWrapper.readHandles.read());
    manage lock.read {
      assert (lock.rlWrapper.readHandles.read() == 2);
      assert(lock.read.rlWrapper.readHandles.read() == lock.rlWrapper.readHandles.read());
      assert(lock.write.rlWrapper.readHandles.read() == lock.rlWrapper.readHandles.read());
    }
  }

}
